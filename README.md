This app is a polyphonic piano that lets you play notes.

It was developed for the iPad Retina/mini 2 resolution; the UI doesn't fit on an iPhone screen.

The sound is a little crackly on my simulator but it runs correctly on my iPad mini 2.

Here�s a snippet of part of my commit log:

commit 36e2df7d116f3d64279f7b8b40a8cdb129bf3fd4
Author: Ryan A <Ryan@Admins-Mac.local>
Date:   Tue Jan 24 06:09:40 2017 +0300

    completed a working iPad Retina prototype

commit b9b5b5cc982d78a29288c45da122bc8565ef9f53
Author: Ryan A <Ryan@Admins-Mac.local>
Date:   Tue Jan 24 04:05:13 2017 +0300

    Started working on actual project

commit 32bed343da68f1c2ecd3216d4a1cca0d10ef61d4
Author: Ryan A <Ryan@Admins-Mac.local>
Date:   Mon Jan 23 20:15:21 2017 +0300

    did some things