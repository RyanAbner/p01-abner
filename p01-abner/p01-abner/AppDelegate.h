//
//  AppDelegate.h
//  p01-abner
//
//  Created by Ryan A on 19/01/2017.

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

