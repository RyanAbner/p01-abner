//
//  plans.m
//  p01-abner
//
//  Created by Ryan A on 24/01/2017.
//  Copyright © 2017 Ryan A. All rights reserved.
//

#import <Foundation/Foundation.h>


/*
ViewController has 12 buttons arranged as a keyboard, one for each note
Each of these is a UIButton property in the ViewController (connect them)
Also create 12 AVAudioPlayer properties, one for each note
Initialize these with the audio files in viewDidLoad
Write a function to play notes; which one plays depends on which button is pressed (Sound should only play as long as button is held down)
Link up that function with each button

*/
