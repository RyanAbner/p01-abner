//
//  ViewController.h
//  p01-abner
//
//  Created by Ryan A on 19/01/2017.


#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController 

@property (nonatomic,retain) IBOutlet UILabel *messageLabel;
@property (nonatomic,retain) IBOutlet UIButton *cButton;
@property (nonatomic,retain) IBOutlet UIButton *dButton;
@property (nonatomic,retain) IBOutlet UIButton *eButton;
@property (nonatomic,retain) IBOutlet UIButton *fButton;
@property (nonatomic,retain) IBOutlet UIButton *gButton;
@property (nonatomic,retain) IBOutlet UIButton *aButton;
@property (nonatomic,retain) IBOutlet UIButton *bButton;
@property (nonatomic,retain) IBOutlet UIButton *c2Button;
@property (nonatomic,retain) IBOutlet UIButton *cSharpButton;
@property (nonatomic,retain) IBOutlet UIButton *dSharpButton;
@property (nonatomic,retain) IBOutlet UIButton *fSharpButton;
@property (nonatomic,retain) IBOutlet UIButton *gSharpButton;
@property (nonatomic,retain) IBOutlet UIButton *aSharpButton;


@property (nonatomic,retain) AVAudioPlayer *cPlayer;
@property (nonatomic,retain) AVAudioPlayer *dPlayer;
@property (nonatomic,retain) AVAudioPlayer *ePlayer;
@property (nonatomic,retain) AVAudioPlayer *fPlayer;
@property (nonatomic,retain) AVAudioPlayer *gPlayer;
@property (nonatomic,retain) AVAudioPlayer *aPlayer;
@property (nonatomic,retain) AVAudioPlayer *bPlayer;
@property (nonatomic,retain) AVAudioPlayer *c2Player;
@property (nonatomic,retain) AVAudioPlayer *cSharpPlayer;
@property (nonatomic,retain) AVAudioPlayer *dSharpPlayer;
@property (nonatomic,retain) AVAudioPlayer *fSharpPlayer;
@property (nonatomic,retain) AVAudioPlayer *gSharpPlayer;
@property (nonatomic,retain) AVAudioPlayer *aSharpPlayer;

-(IBAction)playNote:(id)sender;
-(void) initializePlayers;
-(IBAction)stopNote:(id)sender;
@end

