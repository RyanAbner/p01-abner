//
//  main.m
//  p01-abner
//
//  Created by Ryan A on 19/01/2017.
//  Copyright © 2017 Ryan A. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
