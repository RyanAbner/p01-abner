//
//  ViewController.m
//  p01-abner
//
//  Created by Ryan A on 19/01/2017.
//  Parts of this are copied from this tutorial: http://codewithchris.com/avaudioplayer-tutorial/


#import "ViewController.h"

@interface ViewController () {

}
@end

@implementation ViewController
@synthesize cButton;
@synthesize dButton;
@synthesize eButton;
@synthesize fButton;
@synthesize gButton;
@synthesize aButton;
@synthesize bButton;
@synthesize c2Button;
@synthesize cSharpButton;
@synthesize dSharpButton;
@synthesize fSharpButton;
@synthesize gSharpButton;
@synthesize aSharpButton;


@synthesize cPlayer;
@synthesize dPlayer;
@synthesize ePlayer;
@synthesize fPlayer;
@synthesize gPlayer;
@synthesize aPlayer;
@synthesize bPlayer;
@synthesize c2Player;
@synthesize cSharpPlayer;
@synthesize dSharpPlayer;
@synthesize fSharpPlayer;
@synthesize gSharpPlayer;
@synthesize aSharpPlayer;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    cButton.tag = 1;
    cSharpButton.tag = 2;
    dButton.tag = 3;
    dSharpButton.tag = 4;
    eButton.tag = 5;
    fButton.tag = 6;
    fSharpButton.tag = 7;
    gButton.tag = 8;
    gSharpButton.tag = 9;
    aButton.tag = 10;
    aSharpButton.tag = 11;
    bButton.tag = 12;
    c2Button.tag = 13;
    
    [self initializePlayers];
}


- (void)initializePlayers {
    // Construct URL to sound file
    NSString *cpath = [NSString stringWithFormat:@"%@/c.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *cUrl = [NSURL fileURLWithPath:cpath];
    // Create audio player object and initialize with URL to sound
    cPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:cUrl error:nil];
    
    NSString *cSharppath = [NSString stringWithFormat:@"%@/c#.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *cSharpUrl = [NSURL fileURLWithPath:cSharppath];
    cSharpPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:cSharpUrl error:nil];
    
    NSString *dpath = [NSString stringWithFormat:@"%@/d.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *dUrl = [NSURL fileURLWithPath:dpath];
    dPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:dUrl error:nil];
    
    NSString *dSharppath = [NSString stringWithFormat:@"%@/d#.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *dSharpUrl = [NSURL fileURLWithPath:dSharppath];
    dSharpPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:dSharpUrl error:nil];
    
    NSString *epath = [NSString stringWithFormat:@"%@/e.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *eUrl = [NSURL fileURLWithPath:epath];
    ePlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:eUrl error:nil];
    
    NSString *fpath = [NSString stringWithFormat:@"%@/f.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *fUrl = [NSURL fileURLWithPath:fpath];
    fPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fUrl error:nil];
    
    NSString *fSharppath = [NSString stringWithFormat:@"%@/f#.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *fSharpUrl = [NSURL fileURLWithPath:fSharppath];
    fSharpPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fSharpUrl error:nil];
    
    NSString *gpath = [NSString stringWithFormat:@"%@/g.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *gUrl = [NSURL fileURLWithPath:gpath];
    gPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:gUrl error:nil];
    
    NSString *gSharppath = [NSString stringWithFormat:@"%@/g#.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *gSharpUrl = [NSURL fileURLWithPath:gSharppath];
    gSharpPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:gSharpUrl error:nil];
    
    NSString *apath = [NSString stringWithFormat:@"%@/a.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *aUrl = [NSURL fileURLWithPath:apath];
    aPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:aUrl error:nil];
    
    NSString *aSharppath = [NSString stringWithFormat:@"%@/a#.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *aSharpUrl = [NSURL fileURLWithPath:aSharppath];
    aSharpPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:aSharpUrl error:nil];
    
    NSString *bpath = [NSString stringWithFormat:@"%@/b.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *bUrl = [NSURL fileURLWithPath:bpath];
    bPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:bUrl error:nil];
    
    NSString *c2path = [NSString stringWithFormat:@"%@/c2.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *c2Url = [NSURL fileURLWithPath:c2path];
    c2Player = [[AVAudioPlayer alloc] initWithContentsOfURL:c2Url error:nil];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)playNote:(id)sender {
    NSInteger tag = ((UIButton*)sender).tag;
    if (tag == 1){
        //So we play the note from the start if it's already been pressed.
        [cPlayer setCurrentTime:0];
        [cPlayer play];
    }
    else if (tag == 2){
        [cSharpPlayer setCurrentTime:0];
        [cSharpPlayer play];
    }
    else if (tag == 3){
        [dPlayer setCurrentTime:0];
        [dPlayer play];
    }
    else if (tag == 4){
        [dSharpPlayer setCurrentTime:0];
        [dSharpPlayer play];
    }
    else if (tag == 5){
        [ePlayer setCurrentTime:0];
        [ePlayer play];
    }
    else if (tag == 6){
        [fPlayer setCurrentTime:0];
        [fPlayer play];
    }
    else if (tag == 7){
        [fSharpPlayer setCurrentTime:0];
        [fSharpPlayer play];
    }
    else if (tag == 8){
        [gPlayer setCurrentTime:0];
        [gPlayer play];
    }
    else if (tag == 9){
        [gSharpPlayer setCurrentTime:0];
        [gSharpPlayer play];
    }
    else if (tag == 10){
        [aPlayer setCurrentTime:0];
        [aPlayer play];
    }
    else if (tag == 11){
        [aSharpPlayer setCurrentTime:0];
        [aSharpPlayer play];
    }
    else if (tag == 12){
        [bPlayer setCurrentTime:0];
        [bPlayer play];
    }
    else if (tag == 13){
        [c2Player setCurrentTime:0];
        [c2Player play];
    }
}

-(IBAction)stopNote:(id)sender {
    NSInteger tag = ((UIButton*)sender).tag;
    if (tag == 1)
        [cPlayer stop];
    else if (tag == 2)
        [cSharpPlayer stop];
    else if (tag == 3)
        [dPlayer stop];
    else if (tag == 4)
        [dSharpPlayer stop];
    else if (tag == 5)
        [ePlayer stop];
    else if (tag == 6)
        [fPlayer stop];
    else if (tag == 7)
        [fSharpPlayer stop];
    else if (tag == 8)
        [gPlayer stop];
    else if (tag == 9)
        [gSharpPlayer stop];
    else if (tag == 10)
        [aPlayer stop];
    else if (tag == 11)
        [aSharpPlayer stop];
    else if (tag == 12)
        [bPlayer stop];
    else if (tag == 13)
        [c2Player stop];
}

-(void)playThisNote:(NSString *) fileName {
    
}
@end
